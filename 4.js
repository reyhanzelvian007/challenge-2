function isLowerCase(chr) {
    return chr == chr.toLowerCase() && chr != chr.toUpperCase();
}

function isUpperCase(chr) {
    return chr == chr.toUpperCase() && chr != chr.toLowerCase();
}

function isNumber(chr) {
    return /\d/.test(chr);
}

function isValidPassword (password) {
    if (password.length >= 8) {
        var flag = false; 
        for (let i = 0; i < password.length; i++) {
            if (isUpperCase(password[i]) === true) {
                flag = true;
            }
        }
        if (flag === true) {
            var flag2 = false; 
            for (let i = 0; i < password.length; i++) {
                if (isLowerCase(password[i]) === true) {
                    flag2 = true;
                }
            }
            if (flag2 === true) {
                var flag3 = false;
                for (let i = 0; i < password.length; i++) {
                    if (isNumber(password[i]) === true) {
                        flag3 = true;
                    }
                }
                if (flag3 === true) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

console.log(isValidPassword('Meong2021'));
console.log(isValidPassword('meong2021'));
console.log(isValidPassword('@eong'));
console.log(isValidPassword('Meong2'));
console.log(isValidPassword(0));
console.log(isValidPassword());