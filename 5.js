function getSplitName(personName) {
    if (typeof personName === "string") {
        const fullName = personName.split(" ");
        if (fullName.length > 3) {
            return "This function is only for 3 characters name.";
        }
        else {
            return "{ firstName: " + fullName[0] + ", middleName: " + fullName[1] + ", lastName: " + fullName[2] + " }";
        }
    }
    else {
        return "Error invalid data type."
    }
}

console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));