checkEmail = (email) => {
  var validEmail = /[a-zA-Z0-9]+@[a-zA-Z]+.[a-z]/;

  if (typeof email === "string") {
    if (email.match(validEmail)) {
      return "VALID"
    }
    else {
      return "INVALID"
    }
  } else {
    return "INVALID"
  }
  
}

console.log(checkEmail('apranata@binar.co.id'));
console.log(checkEmail('apranata@binar.com'));
console.log(checkEmail('apranata@binar'));
console.log(checkEmail('apranata'));
console.log(checkEmail(3322));
console.log(checkEmail());