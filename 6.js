function getAngkaTerbesarKedua (dataNumbers) {
    let max1 = -10000;

    if (typeof dataNumbers === "object") {
        for (let i = 0; i < dataNumbers.length; i++) {
            let max2 = dataNumbers[i];

            if (max2 > max1) {
                result = max1;
                max1 = max2;
            } else if (max2 < max1 && max2 > result) {
                result = max2;
            }
        }
        return result;
    } else {
        return "Invalid data type."
    }
}

const dataAngka = [9, 4, 7, 7, 4, 3, 2, 2, 8];

console.log(getAngkaTerbesarKedua(dataAngka));

console.log(getAngkaTerbesarKedua(0));

console.log(getAngkaTerbesarKedua());