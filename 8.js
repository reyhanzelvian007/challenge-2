const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    }
]

function getInfoPenjualan (dataPenjualan) {
    let totalKeuntungan = 0;
    let totalModal = 0;
    let totalJual = 0;
    let persentaseKeuntungan = 0;
    let max1 = -100000;
    let max2 = -100000;
    let bukuTerlaris;
    let penulisTerlaris;

    for (let i = 0; i < dataPenjualan.length; i++) {
      totalKeuntungan = totalKeuntungan + (dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli);
      totalModal = totalModal + dataPenjualan[i].hargaBeli;
      totalJual = totalJual + dataPenjualan[i].hargaJual;
      persentaseKeuntungan = (totalModal/totalJual)*100;

      if (dataPenjualan[i].totalTerjual > max1) {
        max1 = dataPenjualan[i].totalTerjual;
        bukuTerlaris = dataPenjualan[i].namaProduk;
      }
    }

    const calonPenulisTerlaris = [];

    dataPenjualan.forEach(function (a) {
      if (!this[a.penulis]) {
        this[a.penulis] = {penulis: a.penulis, totalTerjual: 0};
        calonPenulisTerlaris.push(this[a.penulis]);
      }
      this[a.penulis].totalTerjual += a.totalTerjual;
    }, Object.create(null));

    for (let j = 0; j < calonPenulisTerlaris.length; j++) {
      if (calonPenulisTerlaris[j].totalTerjual > max2) {
        max2 = calonPenulisTerlaris[j].totalTerjual;
        penulisTerlaris = calonPenulisTerlaris[j].penulis;
      }
    }

    return {
      totalKeuntungan: `Rp${totalKeuntungan}`,
      totalModal: `Rp${totalModal}`,
      persentaseKeuntungan: `${persentaseKeuntungan}%`,
      bukuTerlaris,
      penulisTerlaris
    }
    
}

console.log(getInfoPenjualan(dataPenjualanNovel));